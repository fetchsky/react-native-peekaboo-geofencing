
package com.fetchsky.RNPeekabooGeofencing;

import android.os.Bundle;
import android.util.Log;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.peekaboo.geofencing.PeekabooGeofencingParams;
import com.peekaboo.geofencing.PeekabooGeofencingService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class RNPeekabooGeofencingModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public RNPeekabooGeofencingModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  private Bundle mapToBundle(ReadableMap map) {
    Bundle bundle = new Bundle();
    for (Iterator<Map.Entry<String, Object>> it = map.getEntryIterator(); it.hasNext();) {
      Map.Entry<String, Object> entry = it.next();
      bundle.putString(entry.getKey(), (String) entry.getValue());
    }
    return bundle;
  }

  @ReactMethod
  public void startTracking(ReadableMap params, Promise promise) {
    try {
      Bundle paramsBundle = mapToBundle(params);
      PeekabooGeofencingService.startTracking(reactContext, paramsBundle);
      promise.resolve("1");
    } catch (Exception e) {
      promise.reject(e);
    }
  }

  @ReactMethod
  public void onNotificationOpened(ReadableMap notificationMap) {
    Bundle notification = mapToBundle(notificationMap);
    try {
      PeekabooGeofencingService.onNotificationOpened(notification);
    } catch (Exception e) {
      Log.e("RNPeekabooGeofencing", e.getLocalizedMessage());
    }
  }


  @ReactMethod
  public void onNotificationReceived(ReadableMap notificationMap) {
    Bundle notification = mapToBundle(notificationMap);
    try {
      PeekabooGeofencingService.onNotificationReceived(notification);
    } catch (Exception e) {
      Log.e("RNPeekabooGeofencing", e.getLocalizedMessage());
    }
  }

  @ReactMethod
  public  Map<String, Object> getParamsKeys() {
    Map<String, Object> map = new HashMap<>();
    map.put("SHOW_LOCATION_LOCAL_NOTIFICATIONS", PeekabooGeofencingParams.SHOW_LOCATION_LOCAL_NOTIFICATIONS);
    map.put("BASE_URL", PeekabooGeofencingParams.BASE_URL);
    map.put("FIREBASE_INSTANCE_ID", PeekabooGeofencingParams.FIREBASE_INSTANCE_ID);
    return map;
  }

  @Override
  public String getName() {
    return "RNPeekabooGeofencing";
  }
}
package = JSON.parse(File.read(File.join(__dir__, '../package.json')))

Pod::Spec.new do |s|
  s.name           = "RNPeekabooGeofencing"
  s.version        = package['version']
  s.summary        = package['summary']
  s.description    = package['description']
  s.license        = package['license']
  s.author         = package['author']
  s.homepage       = package['homepage']
  s.platform       = :ios, "7.0"
  s.source         = { :git => "https://gitlab.com/fetchsky/RNPeekabooGeofencing.git", :tag => "master" }
  s.source_files   = "*.{h,m}"
  s.requires_arc   = true
  s.vendored_frameworks = "libs/Geofencing.framework"
  s.dependency "React"
end

  
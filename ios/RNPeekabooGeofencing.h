
#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif

@interface RNPeekabooGeofencing : NSObject <RCTBridgeModule>

+(id)sharedManager;

- (void) onAppStateChange:(NSDictionary *)options;
- (void) onNotificationReceived:(NSDictionary *)options;

@end

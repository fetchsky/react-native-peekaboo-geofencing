
#import <Geofencing/Geofencing.h>
#import "RNPeekabooGeofencing.h"

@implementation RNPeekabooGeofencing{
    NSMutableDictionary* params;
}

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;
}
RCT_EXPORT_MODULE()


+ (id)sharedManager {
    static id sharedMyModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyModel = [[self alloc] init];
    });
    return sharedMyModel;
}

-(void)initialize {
    if (params == nil) {
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [Geofencing.sharedManager trackLocation:self->params];
    });
}

- (void) onAppStateChange:(NSDictionary *)options {
    [self initialize];
}

- (void) onNotificationReceived:(NSDictionary *)options {
    [Geofencing.sharedManager onNotificationReceived:options];
}

- (void) onNotificationOpened:(NSDictionary *)options {
    [Geofencing.sharedManager onNotificationOpened:options];
}

RCT_REMAP_METHOD(setFirebaseIdentifier,
                 setFirebaseIdentifier:(NSString *)fcmId)
{
    [Geofencing.sharedManager setFirebaseInstanceId:fcmId];
}

RCT_REMAP_METHOD(startTracking, startTracking:(NSDictionary *)args
                 resolver: (RCTPromiseResolveBlock)resolve
                 rejecter: (RCTPromiseRejectBlock)reject)
{
    @try {
        if (params == nil) {
            params = [[NSMutableDictionary alloc] initWithDictionary:@{}];
        }
        [params addEntriesFromDictionary:args];
        [self initialize];
        resolve(@"1");
    }
    @catch (NSException * e) {
        reject(@"Geofencing Failed", e.debugDescription, nil);
    }
}


//RCT_REMAP_METHOD(onNotificationOpened, onNotificationOpened:(NSDictionary *)notification)
//{
//    [self onNotificationOpened:notification];
//}
//
//RCT_REMAP_METHOD(onNotificationReceived, onNotificationReceived:(NSDictionary *)notification)
//{
//    [self onNotificationReceived:notification];
//}

@end
  
